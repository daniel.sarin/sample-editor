import { Editor } from 'components';

const App = () => (
  <div className="app">
    <h1>Sample editor</h1>

    <Editor />

    <style jsx>{`
      .app {
        font-family: sans-serif;
        font-size: 16px;
        margin: 50px auto;
        max-width: 800px;
      }
    `}</style>
  </div>
);

export default App;
