# Sample editor

### Setup

1. Install Node.js
2. Run `npm install`

### Development

```
npm run dev
```


### Build

```
npm run build
```
