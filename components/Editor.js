import { Component } from 'react';
import { Button } from 'components';

class Editor extends Component {
  state = {
    blocks: {
      1: {hidden: false},
      2: {hidden: false},
      3: {hidden: false},
    },
    blocksOrder: [1, 2, 3]
  }

  handleBlockToggle = (ev) => {
    const { blocks } = this.state;
    const blockId = parseInt(ev.currentTarget.getAttribute('data-block-id'));
    this.setState({
      blocks: {
        ...blocks,
        [blockId]: {
          hidden: !blocks[blockId]?.hidden
        }
      }
    });
  }

  handleSave = () => {
    console.log('Changes saved.');
  }

  render() {
    const { blocks, blocksOrder } = this.state;

    return (
      <div className="editor">
        <div className="header">
          <Button
            onClick={this.handleSave}
            text="Save changes"
          />
        </div>

        <div className="content">
          {blocksOrder.map(blockId => (
            <div
              className={`block ${blocks[blockId].hidden ? 'hidden' : ''}`}
              key={blockId}
            >
              {`Block ${blockId}${blocks[blockId].hidden ? ' (hidden)' : ''}`}

              <div className="block-actions">
                <Button
                  data-block-id={blockId}
                  onClick={this.handleBlockToggle}
                  text={blocks[blockId].hidden ? 'Show' : 'Hide'}
                />
              </div>
            </div>
          ))}
        </div>

        <style jsx>{`
          .editor {
            background-color: #f1f5fa;
            border-radius: 7px;
            margin-top: 50px;
            padding: 25px;
          }
          .content {
            margin-top: 25px;
          }
          .block {
            background-color: white;
            border-radius: 7px;
            font-size: 20px;
            padding: 25px;
          }
          .block.hidden {
            background-color: #d1d5da;
          }
          .block:not(:first-child) {
            margin-top: 25px;
          }
          .block-actions {
            margin-top: 25px;
            text-align: right;
          }
        `}</style>
      </div>
    );
  }
}

export default Editor;
