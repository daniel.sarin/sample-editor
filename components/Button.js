const Button = ({ icon, onClick, text, ...props }) => (
  <button
    className="button"
    onClick={onClick}
    {...props}
  >
    <span>{text}</span>

    <style jsx>{`
      .button {
        background-color: #245268;
        border: 0;
        border-radius: 7px;
        color: white;
        cursor: pointer;
        font-size: 16px;
        padding: 12px 20px;
        transition: opacity .3s;
      }
      .button:hover {
        opacity: 0.8;
      }
    `}</style>
  </button>
);

export default Button;
